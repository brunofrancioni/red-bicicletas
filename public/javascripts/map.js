var mymap = L.map('main_map').setView([ -34.6083, -58.3712], 13);

L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}', {
    attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
    maxZoom: 18,
    id: 'mapbox/streets-v11',
    tileSize: 512,
    zoomOffset: -1,
    accessToken: 'pk.eyJ1IjoiYnJ1bm9mcmFuY2lvbmkiLCJhIjoiY2tlZzJzN2k3MGhhbzJycnNkaWhqb3U2eCJ9.IBbLv-Uvs6YI3-reYkb2-Q'
}).addTo(mymap);

$.ajax({
    dataType: "json",
    url: "api/bicicletas",
    success: function(result) {
        result.bicicletas.forEach(function(bici) {
            console.log(bici);
            L.marker(bici.ubicacion, {title: bici.id}).addTo(mymap);
        });
    }
})