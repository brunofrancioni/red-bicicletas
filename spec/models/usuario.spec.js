const mongoose = require('mongoose');
const Bicicleta = require('../../models/bicicleta');
const Usuario = require('../../models/usuario');
const Reserva = require('../../models/reserva');

describe('Testing Usuarios', function() {
    beforeEach(function(done) {
        var mongoDB = 'mongodb://localhost/testdb';
        mongoose.connect(mongoDB, { useNewUrlParser: true, useUnifiedTopology: true });

        const db = mongoose.connection;
        db.on('error', console.error.bind(console, 'connection error'));
        db.once('open', function() {
            console.log('We are connected to test database!');

            done();
        });
    });

    afterEach(function(done) {
        Reserva.deleteMany({}, function(err, success) {
            if(err) console.log(err);

            Usuario.deleteMany({}, function(err, success) {
                if(err) console.log(err);

                Bicicleta.deleteMany({}, function(err, success) {
                    if(err) console.log(err);
                    done();
                });
            });
        });
    });

    describe('Cuando un Usuario reserva una bici', function() {
        it('debe existir la reserva', function(done) {
            const usuario = new Usuario({ nombre: 'Ezequiel' });
            usuario.save();
            const bicicleta = new Bicicleta({ code: 1, color: "verde", modelo: "urbana", ubicacion: [-34.5, -54.3] });
            bicicleta.save();

            var hoy = new Date();
            var manana = new Date();
            manana.setDate(hoy.getDate() + 1);
            
            usuario.reservar(bicicleta.id, hoy, manana, function(err, reserva) {
                Reserva.find({}).populate('bicicleta').exec(function(err, reservas) {
                    console.log(reservas);
                    expect(reservas.length).toBe(1);
                    expect(reservas[0].diasDeReserva()).toBe(2);
                    expect(reservas[0].bicicleta.code).toBe(1);
                    expect(reservas[0].usuario.nombre).toBe(usuario.nombre);
                    done();
                });
            });
        });
    });
});