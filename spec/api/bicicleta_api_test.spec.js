const Bicicleta = require('../../models/bicicleta');
const request = require('request');
const server = require('../../bin/www');

describe('Bicicleta API', () => {
    describe('GET BICICLETAS /', () => {
        it('Status 200', () => {
            expect(Bicicleta.allBicis.length).toBe(0);

            var a = new Bicicleta(1, 'rojo', 'urbana', [-34.24513, -58,234143]);
            Bicicleta.add(a);

            request.get('http://localhost:3000/api/bicicletas', (error, response, body) => {
                expect(response.statusCode).toBe(200);
            });
        });
    });

    describe('POST BICICLETAS /create', () => {
        it('Status 200', (done) => {
            expect(Bicicleta.allBicis.length).toBe(1);

            var headers = {'content-type' : 'application/json'};
            var aBici = '{"id": 2, "color": "rojo", "modelo": "urbana", "lat": -34.24513, "lng": -58,234143}';

            request.post({
                headers: headers,
                url: 'http://localhost:3000/api/bicicletas/create',
                body: aBici
            }, (error, response, body) => {
                expect(response.statusCode).toBe(200);
                expect(Bicicleta.findById(2).color).toBe("rojo");
                done();
            });
        });
    });
});